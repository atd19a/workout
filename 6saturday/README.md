
# LIS 4331  |  Advanced Mobile Application Development

## Andrew Dempsey

### Assignment 3 Requirements:

*Completed Steps:*

1. Field to enter U.S dollar amount: 1-100,000
2. Must include notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for Euros, Pesos, and Canadian dollars
5. Must add background color or theme
6. Create and display/launcher icon image
7. Create Splash/Loading Screen

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshots of running application
* Screenshots of skillsets 4, 5 and 6



#### Assignment Screenshots:

|Program:|Program (demo):|
|---|---|
|![Tip Calculator](img/a2_unp.png "A2 Screenshot")|![Tip Calculator Demo](img/a2_calculator.png "A2 Screenshot Demo")|
|   |   |


#### Skillset #4
|Java: Time Conversion|
|---|
|![Time Conversion](img/ss4.png "Skillset 4 Screenshot")|
|   |

#### Skillset #5
|Java: Even or Odd (GUI)|
|---|---|
|![Even or Odd](img/ss5_1.png "Skillset 5 Screenshot")|![Even or Odd](img/ss5_2.png "Skillset 5 Screenshot")|
|![Even or Odd](img/ss5_3.png "Skillset 5 Screenshot")|![Even or Odd](img/ss5_4.png "Skillset 5 Screenshot")|
|   |   |

#### Skillset #6
|Java: Paint Calculator (GUI)|
|---|---|
|![Paint Calculator](img/ss6_1.png "Skillset 6 Screenshot")|![Paint Calculator](img/ss6_2.png "Skillset 6 Screenshot")|
|![Paint Calculator](img/ss6_3.png "Skillset 6 Screenshot")|![Paint Calculator](img/ss6_4.png "Skillset 6 Screenshot")|
|![Paint Calculator](img/ss6_5.png "Skillset 6 Screenshot")|![Paint Calculator](img/ss6_6.png "Skillset 6 Screenshot")|
|   |   |


