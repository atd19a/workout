
# Workout Schedule  |  Monday


## Andrew Dempsey & Nikole Parrilla


### Plan:


#### Plan for:  Andrew Dempsey
*Complete Steps:*

|Chest|Light Tricep|
|---|---|
|1. Bench Press|1. Tricep Extensions (Machine)|
|2. Incline Bench Press|2. Tricep Extensions (Dumbells)|
|3. Decline Bench Press|3. Dips|
|4. Inner Chest Press|4. Tricep Pushups|
|5. Flys|   |
|6. Chest Machine|   |
|7. Lower Chest Pullovers|


1. Field to enter U.S dollar amount: 1-100,000
2. Must include notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for Euros, Pesos, and Canadian dollars
5. Must add background color or theme
6. Create and display/launcher icon image
7. Create Splash/Loading Screen

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshots of running application
* Screenshots of skillsets 4, 5 and 6






