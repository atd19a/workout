
# Workout Schedule 

## Andrew Dempsey & Nikole Parrilla

### Days of the week:

*Schedule as of March (03/01/2021):*

1. [Monday](1monday/README.md "Monday Workout Schedule")
    - Includes workout schedule for Monday
    - Andrew's plan: Chest & Triceps
    - Nikole's plan: Cardio, HIT (with an emphasis on CHEST)
    
2. [Tuesday](2tuesday/README.md "Tuesday Workout Schedule")
    - Includes workout schedule for Tuesday
    - Andrew's plan: Back & Biceps
    - Nikole's plan: Legs & Abs
    
3. [Wednesday](3wednesday/README.md "Wednesday Workout Schedule")
    - Includes workout schedule for Wednesday
    - Andrew's plan: Legs & Abs
    - Nikole's plan: REST DAY

4. [Thursday](4thursday/README.md "Thursday Workout Schedule")
    - Includes workout schedule for Thursday
    - Andrew's plan: Shoulder's & Heavy Triceps
    - Nikole's plan: Cardio, HIT (with an emphasis on BACK)

5. [Friday](5friday/README.md "Friday Workout Schedule")
    - Includes workout schedule for Friday
    - Andrew's plan: Back & Heavy Biceps
    - Nikole's plan: Heavy Legs & Abs

6. [Saturday](6saturday/README.md "Saturday Workout Schedule")
    - Includes workout schedule for Saturday
    - Andrew's plan: Legs & Abs
    - Nikole's plan: Cardio, Shoulder's & Arms

7. [Sunday](7sunday/README.md "Sunday Workout Schedule")
    - Includes cheat meal recipes and ideas
    - REST DAY

